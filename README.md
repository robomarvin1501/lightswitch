Connects a button hooked up to the Raspberry Pi GPIO pins to a HomeAssistant webhook.
This could do anything, I use it as a lightbulb.
Before compiling, one must make a config.txt file in the base of the project, which looks as follows

```
<url for default webhook>
<url for backup webhook>
<pin number>
```

There is a backup webhook because sometimes my https webhook fails me, and the backup means it will still work.

In the ```lightswitch.service``` file, fill in the following details  
```User``` The user that is running this. E.g. the default raspberry pi user is called "pi"  
```ExecStart``` The absolute path to the binary file  
