use rust_gpiozero::*;
use std::{error::Error, thread, time::Duration};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let targets: Vec<&str> = include_str!("../config.txt").lines().collect();
    let default = targets.get(0).unwrap();
    let backup = targets.get(1).unwrap();
    let pin_number: u8 = targets.get(2).unwrap().parse().unwrap();

    let mut button = Button::new(pin_number);
    let client = reqwest::Client::new();
    loop {
        button.wait_for_release(None);
        let r = client
            .post(*default)
            .send()
            .await;
        match r {
            Ok(_) => (),
            Err(_) => {
                let _ = client.post(*backup)
                    .send()
                    .await?;
            }
        }
        thread::sleep(Duration::from_millis(1000));
    }
}
